﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public GameObject lightswitch; // the light object that is attached in unity
    public int onoff = 0; // a test value that was used  as a different way to check and see if the lights were turning on and off.
    

	// Use this for initialization
	void Start () {
        lightswitch.GetComponent<Light>().enabled = false; //sets all current light componets to off
    }
	
	// Update is called once per frame
	void Update () {
        //turnOnOrOff();
    }

    //public void turnOnOrOff()
    //{
    //    if (onoff % 2 == 0)
    //    {
    //        lightswitch.GetComponent<Light>().enabled = false;
    //    }
    //    else
    //    {
    //        lightswitch.GetComponent<Light>().enabled = true;
    //    }
    //}
    public void turnOn()
    {
        lightswitch.GetComponent<Light>().enabled = true; // when called this turns the light componet back to On.
    }
}

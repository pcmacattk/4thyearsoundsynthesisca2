﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoCollisions : MonoBehaviour {

    public MidiNumbers midiNumbers; // instance of the midi numbers script so i can access its methods and variables, the instance of the object is assaigned in unity
    public GameObject currentKey; // the current key that the player will step on/ walk on
    public SendMidi sendMidi; // instance of the Send midi script so i can access its methods and variables, the instance of the object is assaigned in unity

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void OnTriggerEnter(Collider player) // i have the piano keys set up as triggers so when another collider(e.g the player) hits them / enters them
    {
        if (currentKey.name.Equals("Black A"))
        {
            playKeyBlackA();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        if (currentKey.name.Equals("A"))
        {
            playKeyA();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("Black AB"))
        {
            playKeyAB();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("B"))
        {
            playKeyB();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("C"))
        {
            playKeyC();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("Black CD"))
        {
            playKeyCD();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("D"))
        {
            playKeyD();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("Black DE"))
        {
            playKeyDE();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("E"))
        {
            playKeyE();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("F"))
        {
            playKeyF();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("Black FG"))
        {
            playKeyFG();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        else if (currentKey.name.Equals("G"))
        {
            playKeyG();
            sendMidi.sendMidiNumber();
            sendMidi.sendVolumeNumber();
        }
        Debug.Log(player.name);
    }
    public void playKeyBlackA()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber - 4);
    }
    public void playKeyA()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber - 3);
    }
    public void playKeyAB()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber - 2);
    }
    public void playKeyB()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber - 1);
    }
    public void playKeyC()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber);
    }
    public void playKeyCD()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 1);
    }
    public void playKeyD()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 2);
    }
    public void playKeyDE()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 3);
    }
    public void playKeyE()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 4);
    }
    public void playKeyF()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 5);
    }
    public void playKeyFG()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 6);
    }
    public void playKeyG()
    {
        midiNumbers.midiNumberBeingSent = (midiNumbers.currentmidiMiddleCNumber + 7);
    }
}

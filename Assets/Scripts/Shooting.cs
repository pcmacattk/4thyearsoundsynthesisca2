﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {
    // this script uses raycasts in order to toggle switches and play piano keys
    
    public float range = 100f;
    public float fireRate = 15f;

    public MidiNumbers MidiNumbers; // instance of the midi numbers script so i can access its methods and variables, the instance of the object is assaigned in unity
    public PianoCollisions pianoCollisions; // instance of the piano collisions script so i can access its methods and variables, the instance of the object is assaigned in unity
    public SendMidi sendMidi; // instance of the sendMidi script so i can access its methods and variables, the instance of the object is assaigned in unity
    public Camera fpsCam; // camera from which the raycast will shoot from, its attached to the first person camera in unity

    private float nextTimeToFire = 0f;
    // Update is called once per frame
    void Update()
    {

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire) //unity defaults left mouse click as "Fire1"
        {
            nextTimeToFire = Time.time + 2f / fireRate; //makes the fire rate into a semi automatic if needed,with a change to the nextTimeToFire, mainly used it for testing.
            Shoot();
        }
    }

    void Shoot() //Main method that handles the raycasts and what to do when the raycast hits a specific target or object
    {
        RaycastHit hit; // stores info about what object or non-object the Ray hits.
        //muzzleFlash.Play();
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {

            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();
            //if (target != null)
            //{

            //}
            if (hit.transform.GetComponent<Target>() == true)
            {
                if (target.gameObject.name.Contains("Switch"))
                {
                    MidiNumbers.resetVolume(); 

                    if (target.gameObject.name.Contains("Switch +"))
                    {
                        if (MidiNumbers.currentmidiMiddleCNumber == 96)
                        {
                            Debug.Log("Cant Increase any further");
                        }
                        else if ((MidiNumbers.currentmidiMiddleCNumber == 84 && target.gameObject.name.Contains("Switch +2")) || (MidiNumbers.currentmidiMiddleCNumber == 84 && target.gameObject.name.Contains("Switch +3")))
                        {
                            Debug.Log("Cant Increase by +2 or +3");
                        }
                        else if (MidiNumbers.currentmidiMiddleCNumber == 72 && target.gameObject.name.Contains("Switch +3"))
                        {
                            Debug.Log("Cant Increase by +3");
                        }
                        else
                        {
                            if (target.gameObject.name.Contains("Switch +1"))
                            {
                                MidiNumbers.currentmidiMiddleCNumber = MidiNumbers.currentmidiMiddleCNumber + 12;
                                
                                MidiNumbers.i = 0;
                                target.turnOn();
                                MidiNumbers.checkVolume();
                                Debug.Log(MidiNumbers.currentmidiMiddleCNumber);
                            }
                            else if (target.gameObject.name.Contains("Switch +2"))
                            {
                                MidiNumbers.currentmidiMiddleCNumber = MidiNumbers.currentmidiMiddleCNumber + 24;
                               
                                MidiNumbers.i = 1;
                                target.turnOn();
                                MidiNumbers.checkVolume();
                                Debug.Log(MidiNumbers.currentmidiMiddleCNumber);
                            }
                            else if (target.gameObject.name.Contains("Switch +3"))
                            {
                                MidiNumbers.currentmidiMiddleCNumber = MidiNumbers.currentmidiMiddleCNumber + 36;
                                MidiNumbers.i = 2;
                                target.turnOn();
                                MidiNumbers.checkVolume();
                                
                            }
                        }
                    }
                    if (target.gameObject.name.Contains("Switch -"))
                    {
                        if (MidiNumbers.currentmidiMiddleCNumber == 24)
                        {
                            Debug.Log("Cant Decrease any further");
                        }
                        else if ((MidiNumbers.currentmidiMiddleCNumber == 36 && target.gameObject.name.Contains("Switch -2")) || (MidiNumbers.currentmidiMiddleCNumber == 36 && target.gameObject.name.Contains("Switch -3")))
                        {
                            Debug.Log("Cant Decrease by -2 or -3");
                        }
                        else if (MidiNumbers.currentmidiMiddleCNumber == 48 && target.gameObject.name.Contains("Switch -3"))
                        {
                            Debug.Log("Cant Decrease by -3");
                        }
                        else
                        {
                            if (target.gameObject.name.Contains("Switch -1"))
                            {
                                MidiNumbers.currentmidiMiddleCNumber = MidiNumbers.currentmidiMiddleCNumber - 12;
                                
                                MidiNumbers.i = 3;
                                target.turnOn();
                                MidiNumbers.checkVolume();
                                Debug.Log(MidiNumbers.currentmidiMiddleCNumber);
                            }
                            else if (target.gameObject.name.Contains("Switch -2"))
                            {
                                MidiNumbers.currentmidiMiddleCNumber = MidiNumbers.currentmidiMiddleCNumber - 24;
                                
                                MidiNumbers.i = 4;
                                target.turnOn();
                                MidiNumbers.checkVolume();
                                Debug.Log(MidiNumbers.currentmidiMiddleCNumber);
                            }
                            else if (target.gameObject.name.Contains("Switch -3"))
                            {
                                MidiNumbers.currentmidiMiddleCNumber = MidiNumbers.currentmidiMiddleCNumber - 36;
                                MidiNumbers.i = 5;
                                target.turnOn();
                                MidiNumbers.checkVolume();
                                
                            }
                        }
                    }
                    Debug.Log("target hit");
                    midiNumbersAndPd();
                }

            }
            else
            {
                Debug.Log(MidiNumbers.midiNumberBeingSent);
                if (hit.transform.name.Equals("Black A")) //all following if statements check the objects name against the name of the keys, then it calls the corresponding method to change the midi note to its key equivalent and then calls the send midi number method for the midi number and the volume to PD
                {

                    pianoCollisions.playKeyBlackA();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("A"))
                {
                    pianoCollisions.playKeyA();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("B"))
                {
                    pianoCollisions.playKeyB();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("Black AB"))
                {
                    pianoCollisions.playKeyAB();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("C"))
                {
                    pianoCollisions.playKeyC();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("Black CD"))
                {
                    pianoCollisions.playKeyCD();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("D"))
                {
                    pianoCollisions.playKeyD();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("Black DE"))
                {
                    pianoCollisions.playKeyDE();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("E"))
                {
                    pianoCollisions.playKeyE();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("F"))
                {
                    pianoCollisions.playKeyF();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("Black FG"))
                {
                    pianoCollisions.playKeyFG();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
                else if (hit.transform.name.Equals("G"))
                {
                    pianoCollisions.playKeyG();
                    sendMidi.sendMidiNumber();
                    sendMidi.sendVolumeNumber();
                }
            }



            //if (hit.rigidbody != null)
            //{
            //    //hit.rigidbody.AddForce(-hit.normal * impactForce);
            //}

            //GameObject impactObject = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            //Destroy(impactObject, 1f);
        }
    }

    public void midiNumbersAndPd() //this method calls the midiNumbers methods that clears the renderers of all midi middle C number displays and then calls the method that checks if the current midi middle C number matches with a display number and then displays it.
    {
        MidiNumbers.cleanNumbers();
        MidiNumbers.displayMidiNumbers();

    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidiNumbers : MonoBehaviour {

    public GameObject[] midinums; // array of gameObjects that are the text on the right wall saying what number middle C currently is
    public int currentmidiMiddleCNumber = 60;
    public int midiNumberBeingSent = 0;
    public int volume = 1;
    public GameObject[] targets;//array of targets(the lights to switch on and off)
    public int i = 0;

    // Use this for initialization
    void Start () {
        cleanNumbers();
        displayMidiNumbers();
        InvokeRepeating("clearLights", 0.5f, 2.0f); // this calls the clearLights method every 2 seconds after unity starts up,
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void cleanNumbers() // disables the renderer on all of the midinums gameObject array
    {
        for (int i = 0; i < midinums.Length; i++)
        {
            midinums[i].GetComponent<Renderer>().enabled = false;
        }
    }

    public void displayMidiNumbers()//enables the renderer of the gameObject within the midinums array whose number matches the current midi Middle C number
    {
        for (int i = 0; i < midinums.Length; i++)
        {
            if (midinums[i].name.Contains(currentmidiMiddleCNumber.ToString()))
            {
                midinums[i].GetComponent<Renderer>().enabled = true;
            }
        }
    }

    public void clearLights() // disables the light componet on each target object
    {
        targets[i].GetComponent<Light>().enabled = false;
        
        Debug.Log("clearing");
        
    }
    public void checkVolume() // checks if the midi middle C number is below 60 and re-adjusts the volume number being sent so that the notes being played can be heard.
    {
        if (currentmidiMiddleCNumber == 48)
        {
            volume = volume + 3;
        }
        else if (currentmidiMiddleCNumber == 36)
        {
            volume = volume + 5;
        }
        else if (currentmidiMiddleCNumber == 24)
        {
            volume = volume + 9;
        }
        else
        {
            volume = 1;
        }
    }
    public void resetVolume() // reset method that sets the volume number back to its original value of 1
    {
        volume = 1;
    }

}

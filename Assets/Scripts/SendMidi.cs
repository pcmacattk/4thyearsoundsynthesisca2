﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMidi : MonoBehaviour
{
    public OSC osc;
    public MidiNumbers midiNumbers;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {



    }

    public void sendMidiNumber() // sends an oscMessage to pd with the midi number of the key that is currently being played.
    {
        OscMessage message = new OscMessage();

        message.address = "/UpdateMidi";
        message.values.Add(midiNumbers.midiNumberBeingSent);
        osc.Send(message);
    }
    public void sendVolumeNumber() // sends an oscMessage to pd with the volume number of the key that is currently being played.
    {
        
        OscMessage message = new OscMessage();

        message.address = "/UpdateVolume";
        message.values.Add(midiNumbers.volume);
        osc.Send(message);
        
    }
}
